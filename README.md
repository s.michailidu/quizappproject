# Velká práce

Projekt do předmětu SSP - velká práce. Django projekt na téma web pro správu kvízů.

## Instalace
1. Naklonování projektu
    ```
    git clone https://gitlab.com/s.michailidu/quizappproject.git
    ```
1. Vytvoření a aktivace virtuálního prostředí
    ```
    virtualenv venv
    source venv/bin/activate
    ```
1. Instalace balíčků
    ```
    pip install -r requirements.txt
    ```
1. Run
    ```
    python manage.py runserver
    ```

