from django.contrib import admin
from .models import Quiz, Question, Topic, Registration, Team, Points

admin.site.register(Quiz)
admin.site.register(Question)
admin.site.register(Topic)
admin.site.register(Registration)
admin.site.register(Team)
admin.site.register(Points)