from django.db import models

class Quiz(models.Model):
    start = models.DateTimeField('start time', auto_now_add=False)
    end = models.DateTimeField('end time', auto_now_add=False)

    def __str__(self):
        if self.start.strftime("%d. %m. %Y") == self.end.strftime("%d. %m. %Y"):
            return self.start.strftime("%d. %m. %Y")
        else:
            return self.start.strftime("%d. %m. %Y") + ' - ' + self.end.strftime("%d. %m. %Y")


class Topic(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.TextField()
    image = models.TextField(blank=True)

    def __str__(self):
        return self.name

class Question(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    question_text = models.TextField()
    answer = models.TextField()
    max_points = models.PositiveSmallIntegerField()

    def __str__(self):
        return str(self.topic) + ' - ' + self.question_text

class Team(models.Model):
    name = models.CharField(max_length=50)
    captain_name = models.CharField(max_length=50)
    captain_surname = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=15)
    email = models.CharField(max_length=100)
    number_of_members = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name

class Registration(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    time = models.DateTimeField()

    def __str__(self):
        return str(self.team) + ', ' + str(self.quiz)

class Points(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, null=True, on_delete=models.SET_NULL)
    number_of_points = models.PositiveSmallIntegerField()
