from django import forms
from .models import Topic, Quiz, Question, Team, Points, Registration

class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        exclude = ['quiz']

class QuizForm(forms.ModelForm):
    start = forms.DateTimeField(
        input_formats = ['%Y-%m-%dT%H:%M'],
        widget = forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                'class': 'form-control'},
            format='%Y-%m-%dT%H:%M')
        )
    end = forms.DateTimeField(
        input_formats = ['%Y-%m-%dT%H:%M'],
        widget = forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                'class': 'form-control'},
            format='%Y-%m-%dT%H:%M')
        )
    class Meta:
        model = Quiz
        fields = ('start','end')

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        exclude = ['topic']

class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ('name','captain_name','captain_surname','phone_number','email','number_of_members')

class PointsForm(forms.Form):
    def __init__(self, *args, **kwargs):
        teams = kwargs.pop('teams')
        question = kwargs.pop('question')
        super(PointsForm, self).__init__(*args, **kwargs)
        counter = 1
        for i, team in enumerate(teams):
            try:
                p = Points.objects.get(team=team, question=question)
                init_points = p.number_of_points
            except Points.DoesNotExist:
                init_points = 0
            self.fields[str(team.id)] = forms.IntegerField(
                label=team.name,
                max_value=question.max_points,
                min_value=0,
                initial=init_points
                )

class RegistrationForm(forms.ModelForm):
    class Meta:
        model = Registration
        exclude = ['time']
