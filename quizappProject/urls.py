"""django2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from quizapp import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('teams', views.teams, name='teams'),
    path('quiz/<int:quiz_id>/', views.quiz, name='quiz'),
    path('quiz/<int:quiz_id>/results/', views.results, name='results'),
    path('quiz/<int:quiz_id>/addtopic', views.addtopic, name='addtopic'),
    path('quiz/<int:quiz_id>/newtopic/', views.newtopic, name='newtopic'),
    path('newquiz/', views.newquiz, name='newquiz'),
    path('addquiz/', views.addquiz, name='addquiz'),
    path('deletequiz/<int:quiz_id>/', views.deletequiz, name='deletequiz'),
    path('editquiz/<int:quiz_id>/', views.editquiz, name='editquiz'),
    path('edittopic/<int:topic_id>/', views.edittopic, name='edittopic'),
    path('topic/<int:topic_id>', views.topic, name='topic'),
    path('deletetopic/<int:topic_id>/', views.deletetopic, name='deletetopic'),
    path('quiz/topic/<int:topic_id>/newquestion/', views.newquestion, name='newquestion'),
    path('quiz/topic/<int:topic_id>/addquestion/', views.addquestion, name='addquestion'),
    path('question/<int:question_id>', views.question, name='question'),
    path('deletequestion/<int:question_id>/', views.deletequestion, name='deletequestion'),
    path('editquestion/<int:question_id>/', views.editquestion, name='editquestion'),
    path('newteam/', views.newteam, name='newteam'),
    path('addteam/', views.addteam, name='addteam'),
    path('newregistration/', views.createnewregistration, name='createnewregistration'),
    path('addregistration/', views.createaddregistration, name='createaddregistration'),
    path('team/<int:team_id>/', views.team, name='team'),
    path('deleteteam/<int:team_id>/', views.deleteteam, name='deleteteam'),
    path('editteam/<int:team_id>/', views.editteam, name='editteam'),
    path('registrations/<int:team_id>/', views.registrations, name='registrations'),
    path('registrationsbyquiz/<int:quiz_id>/', views.registrationsbyquiz, name='registrationsbyquiz'),
    path('addregistration/<int:team_id>/<int:quiz_id>/<int:view_quiz>/', views.addregistration, name='addregistration'),
    path('deleteregistration/<int:team_id>/<int:quiz_id>/<int:view_quiz>/', views.deleteregistration, name='deleteregistration'),
    path('quiz/<int:question_id>/addpoints', views.addpoints, name='addpoints'),
    path('quiz/<int:question_id>/newpoints/', views.newpoints, name='newpoints'),
]
